import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NavbarComponent } from './components/navbar/navbar.component';
import { SignupComponent } from './pages/signup/signup.component';
import { LoginComponent } from './pages/login/login.component';

import { MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule} from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { HomeComponent } from './pages/home/home.component';
import { MatCardModule} from '@angular/material/card';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatIconModule} from '@angular/material/icon';
import { authInterceptorProviders } from './services/auth.interceptor';
import { FooterComponent } from './components/footer/footer.component';
import { NosotrosComponent } from './pages/nosotros/nosotros.component';
import { AgendacitaComponent } from './pages/agendacita/agendacita.component';
import { SignupMedicoComponent } from './pages/signup-medico/signup-medico.component';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule, } from '@angular/material/core';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { historia_clinica } from './models/historia_clinica';
import { MatDividerModule} from '@angular/material/divider';
import { MatListModule} from '@angular/material/list';
import { MatExpansionModule} from '@angular/material/expansion';
import { BuscarHistoriaComponent } from './pages/buscarhistoria/buscarhistoria.component';
import { RegistrarHistoriaComponent } from './pages/registrarhistoria/registrarhistoria.component';
import { paginacion } from './pages/buscarhistoria/paginacion.pipe';
import { accesousuarios } from './models/accesousuarios';
import {MatPaginatorModule} from '@angular/material/paginator';
import { user } from './models/user';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SignupComponent,
    LoginComponent,
    HomeComponent,
    FooterComponent,
    NosotrosComponent,
    AgendacitaComponent,
    SignupMedicoComponent,
    BuscarHistoriaComponent,
    RegistrarHistoriaComponent,
    paginacion


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    HttpClientModule,
    MatSnackBarModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDividerModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatListModule,
    MatExpansionModule
  ],
  providers: [authInterceptorProviders,historia_clinica,accesousuarios,user],
  bootstrap: [AppComponent]
})
export class AppModule { }
