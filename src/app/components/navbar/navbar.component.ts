import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { accesousuarios } from 'src/app/models/accesousuarios';
import { IpService } from 'src/app/services/Ip.service';
import { AccesoUsuariosService } from 'src/app/services/accesousuarios.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit { //Un enlace de ciclo de vida que se llama después de que Angular haya inicializado todas las propiedades vinculadas a datos de una directiva. Defina un método ngOnInit() para manejar cualquier tarea de inicialización adicional.

  isLoggedIn = false;
  user:any = null;
  banderaResgistrar = false;
  
  constructor(public login:LoginService,
              private accesousuariosService:AccesoUsuariosService,
              private ipService: IpService,
              public accesoslogs: accesousuarios,
              private router:Router,) { }

  ngOnInit(): void {
    this.isLoggedIn = this.login.isLoggedIn();
    this.user = this.login.getUser();
    this.login.loginStatusSubjec.asObservable().subscribe(
      data => {
        this.isLoggedIn = this.login.isLoggedIn();
        this.user = this.login.getUser();
      }
    )   
    this.Registrar();
  }

  public logout(){
    Swal.fire({
      title: '¿Está seguro?',
      text: "¡La sesion se cerrara!",
      icon: 'warning',
      confirmButtonColor: '#3d9970',
      showConfirmButton: true,
      showCancelButton: true,
      denyButtonText: `Don't save`,
      confirmButtonText: 'Ok',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    }).then((result) => {
      if (result.isConfirmed) {
        let loginTime = new Date();
        let loginTimeString = loginTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true });
        this.ipService.getIpAddress().subscribe(response => {   
          let userStr = localStorage.getItem('user');  
          if(userStr != null){
            let user = JSON.parse(userStr);
            let ipAddress = JSON.stringify(response);
            this.accesoslogs.nombre=user.nombre
            this.accesoslogs.apellido=user.apellido
            this.accesoslogs.identificacion=user.identificacion
            this.accesoslogs.username=user.username
            this.accesoslogs.ip=ipAddress
            this.accesoslogs.accion="Logout"
            this.accesoslogs.hora=loginTimeString
            this.accesoslogs.fecha=loginTime.toLocaleDateString()
            this.accesousuariosService.enviarLogsAcceso(this.accesoslogs).subscribe((logs) =>{
              this.login.logout();
              this.router.navigateByUrl('/login');
          })
          }                 
          });
          
          
      }        
    })
}



  Registrar():void{
    if(this.user==null||this.user== ''){
      this.banderaResgistrar=false;
    }
    else{
      if(this.user.authorities[0].authority=="ADMIN"){
        this.banderaResgistrar=true;
      }else{
        this.banderaResgistrar=false; 
      }
    }  
   
  }

}
