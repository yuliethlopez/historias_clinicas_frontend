import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import baserUrl from './helper';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
  export class DiagnosticoService{

    constructor(private httpClient: HttpClient) { }

     public getDiagnosticosxHistoria(id:number):Observable<any>{
         return this.httpClient.get(`${baserUrl}/api/diagnosticos/historia/${id}`);
       }

    public getDiagnostico(descripcion:any):Observable<any>{    
        return this.httpClient.get(`${baserUrl}/api/diagnosticos/${descripcion}`)       
      }

    public search(query: string):Observable<any> {
        return this.httpClient.get(`${baserUrl}/search/${query}`);
      }
    
  }

