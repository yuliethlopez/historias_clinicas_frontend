import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import baserUrl from './helper';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {


    constructor(private httpClient: HttpClient) { }

    public añadirUsuario(user:any){
      return this.httpClient.post(`${baserUrl}/usuarios/`,user);
    }

    public añadirAdmin(user:any){
      return this.httpClient.post(`${baserUrl}/usuarios/admin`,user);
    }

    public getUsuario(identificacion:any):Observable<any>{    
      return this.httpClient.get(`${baserUrl}/usuarios/${identificacion}`)       
    } 

}
