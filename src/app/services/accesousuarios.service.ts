import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import baserUrl from './helper';
import { accesousuarios } from '../models/accesousuarios';


@Injectable({
  providedIn: 'root'
})
export class AccesoUsuariosService{

   constructor(private http:HttpClient) { }


   public enviarLogsAcceso(accesousuario:accesousuarios): Observable<any>{
    return this.http.post<accesousuarios>(`${baserUrl}/logs/accesousuarios`,accesousuario);
  }

   public getlosg():Observable<any>{
       return this.http.get(`${baserUrl}/logs/accesousuarios`);
    }
}