import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import baserUrl from './helper';


@Injectable({
  providedIn: 'root'
})
export class IpService {
    
    constructor(private http: HttpClient) {}

    getIpAddress() {
        return this.http.get(`${baserUrl}/logs`);
    }
}


