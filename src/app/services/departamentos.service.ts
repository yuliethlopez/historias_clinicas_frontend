import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import baserUrl from './helper';
import { Observable } from 'rxjs';
import { historia_clinica } from '../models/historia_clinica';

@Injectable({
  providedIn: 'root'
})
export class DepartamentosService {

    constructor(private httpClient: HttpClient) { }

    public getDepartamentos():Observable<any>{
       return this.httpClient.get(`${baserUrl}/api/departamentos`);
     }

    public getDepartamento(codigo:any):Observable<any>{    
        return this.httpClient.get(`${baserUrl}/api/departamentos/${codigo}`,)       
      }

      public getMunicipioxDepartamento(codigo_departamento:any):Observable<any>{    
        return this.httpClient.get(`${baserUrl}/api/municipios/${codigo_departamento}`,)       
      }

}