import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import baserUrl from './helper';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HistoriaClinicaService {


    constructor(private httpClient: HttpClient) { }

    public añadirHistoria(historia_clinica:any ){
      return this.httpClient.post(`${baserUrl}/api/historias/
      ${historia_clinica.codigo_departamento}/
      ${historia_clinica.codigo_municipio}`,historia_clinica);
    }

    public enviarDiagnostico(diagnostico:any){
      return this.httpClient.post(`${baserUrl}/api/historias/enviardiagnosticos`,diagnostico);
    }

    public enviarTratamiento(tratamiento:any){
      return this.httpClient.post(`${baserUrl}/api/historias/enviartratamiento`,tratamiento);
    }


    // public getHistorias():Observable<any>{
    //   return this.httpClient.get(`${baserUrl}/api/historias/`);
    // }

    public getHistoria(identificacion:any):Observable<any>{    
      return this.httpClient.get(`${baserUrl}/api/historias/${identificacion}`)       
    }


} 