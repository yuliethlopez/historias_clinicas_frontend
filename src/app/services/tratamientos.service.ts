import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import baserUrl from './helper';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
  export class TratamientoService{

    constructor(private httpClient: HttpClient) { }

    public getTratamiento(descripcion:any):Observable<any>{    
        return this.httpClient.get(`${baserUrl}/api/tratamientos/${descripcion}`)       
      }

      public getTratamientoxHistoria(id:number):Observable<any>{
        return this.httpClient.get(`${baserUrl}/api/tratamientos/historia/${id}`);
      }

  }