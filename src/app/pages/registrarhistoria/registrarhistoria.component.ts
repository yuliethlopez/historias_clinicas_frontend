import { Component, OnInit } from '@angular/core';
import { HistoriaClinicaService } from 'src/app/services/historia_clinica.service';
import { DepartamentosService } from 'src/app/services/departamentos.service';
import  Swal  from 'sweetalert2';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { historia_clinica } from 'src/app/models/historia_clinica';
import { DiagnosticoService} from 'src/app/services/diagnosticos.service'
import { TratamientoService } from 'src/app/services/tratamientos.service';
import { UserService } from 'src/app/services/user.service';
import { IpService } from 'src/app/services/Ip.service';
import { accesousuarios } from 'src/app/models/accesousuarios';
import { AccesoUsuariosService } from 'src/app/services/accesousuarios.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './registrarhistoria.component.html',
  styleUrls: ['./registrarhistoria.component.css']
})
export class RegistrarHistoriaComponent implements OnInit {


  public listamunicipios: any[] = [];
  public listadepartamentos: any[] = [];
  public listadiagnosticos: any[] = [];
  public fecha_nacimiento = new Date();
  public filterDiagnosticos = "";
  public filterTratamientos = "";
  public cedula = "";
  public listadodediagnosticosactuales: any[] = [];
  public listadodetratamientosactuales: any[] = [];
  public listatratamiento: any[] = [];
  public step = 0;

  constructor(private HistoriaClinicaService:HistoriaClinicaService,
              private DepartamentosService:DepartamentosService,
              private snack:MatSnackBar,
              private router:Router,
              public  historia_clinica:historia_clinica,
              private DiagnosticoService:DiagnosticoService,
              private TratamientoService:TratamientoService,
              private userservice:UserService,
              private ipService: IpService,
              public accesoslogs: accesousuarios,
              private accesousuariosService:AccesoUsuariosService,
              ) { }

  ngOnInit(): void {
    this.TraerInfoNecesariaBack();
  }

  imc(){
    if(this.historia_clinica.talla != null && this.historia_clinica.peso!=null){
      this.historia_clinica.imc = this.historia_clinica.peso/this.historia_clinica.talla
    }
  }

  buscarmunicipios(codigo_departamento: number){
    this.DepartamentosService.getMunicipioxDepartamento(codigo_departamento).subscribe(
      data => {
        this.listamunicipios = data;
         }, error => {
        console.log(error);
      })
  }

  busquedausuario(cedula:string){
    this.userservice.getUsuario(cedula).subscribe(
      data => {        
        const fecha = new Date(data.fecha_nacimiento);
        
        const anio = fecha.getFullYear();
        const mes = fecha.getMonth();
        const dia = fecha.getDate();

        const fechaSoloDiaMesAnio = new Date(anio, mes, dia);

        let fechaorganizada = fecha.toLocaleDateString()
        const fecha_nacimiento = new Date(fechaorganizada);

        this.historia_clinica.apellido=data.apellido
        this.historia_clinica.nombre=data.nombre
        this.historia_clinica.identificacion=data.identificacion
        this.historia_clinica.fecha_nacimiento = fechaorganizada
        this.historia_clinica.lugar_nacimiento=data.lugar_nacimiento
        this.historia_clinica.edad=data.edad
        this.historia_clinica.sexo=data.sexo
        this.historia_clinica.eps=data.eps
        this.historia_clinica.direccion=data.direccion
        this.historia_clinica.regimen=data.regimen
        this.historia_clinica.estado_civil=data.estado_civil
        this.historia_clinica.email=data.email
        this.historia_clinica.telefono=data.telefono
         }, error => {
        console.log(error);
      })
  }

  TraerInfoNecesariaBack(){
    this.DepartamentosService.getDepartamentos().subscribe(
      data => {
        this.listadepartamentos = data;
      }, error => {
        console.log(error);
      })
  }

  agregardiagnostico(diagnosticos:any){
    this.listadodediagnosticosactuales.push(diagnosticos);
  }

  agregartratamiento(tratamiento:any){
    this.listadodetratamientosactuales.push(tratamiento);
  }

  buscarTratamientos(busque:any){
    this.TratamientoService.getTratamiento(busque).subscribe(
      data => {
        this.listatratamiento = data;
      }, error => {
        console.log(error);
      })
  }

  busquedaDiagosticos(busque:String)
  {
    this.DiagnosticoService.getDiagnostico(busque).subscribe(
      data => {
        this.listadiagnosticos = data;
        console.log(this.listadiagnosticos);
      }, error => {
        console.log(error);
      })
  }

  agregardiagnosticosHistoria(){
    this.historia_clinica.diagnostico = this.listadodediagnosticosactuales
    Swal.fire('Diagnosticos agregados','Los diagnosticos han sido registrados con exito en la Historia Clinica','success');
     }

  agregarTratamientoHistoria(){
    this.historia_clinica.tratamiento = this.listadodetratamientosactuales
    Swal.fire('tratamientos agregados','Los tratamientos han sido registrados con exito en la Historia Clinica','success');
    
  }

  eliminarTratamientosHistoria(id:any){
    const index =  this.listadodetratamientosactuales.findIndex((tratamientos)=>{
      return tratamientos.id==id
    })    
    Swal.fire({
      title: '¿Está seguro?',
      text: "¡Eliminara el tratamiento!",
      icon: 'warning',
      confirmButtonColor: '#3d9970',
      showConfirmButton: true,
      showCancelButton: true,
      denyButtonText: `Don't save`,
      confirmButtonText: 'Ok',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    }).then((result) => {
      if (result.isConfirmed) {
        this.listadodetratamientosactuales.splice(index,1)    
      }        
    })
  }

  eliminardiagnosticosHistoria(id:any){
    const index =  this.listadodediagnosticosactuales.findIndex((diagnosticos)=>{
      return diagnosticos.id==id
    })    
    Swal.fire({
      title: '¿Está seguro?',
      text: "¡Eliminara el diagnostico!",
      icon: 'warning',
      confirmButtonColor: '#3d9970',
      showConfirmButton: true,
      showCancelButton: true,
      denyButtonText: `Don't save`,
      confirmButtonText: 'Ok',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    }).then((result) => {
      if (result.isConfirmed) {
        this.listadodediagnosticosactuales.splice(index,1)    
      }        
    })
  }

  enviardepartamento(codigo_departamento: number){
    this.DepartamentosService.getDepartamento(codigo_departamento).subscribe(
      data => {
        this.historia_clinica.codigo_departamento = data;
      }, error => {
        console.log(error);
      })
  }

  // addFechadeNacimiento(event: MatDatepickerInputEvent<Date>) {
  //   let dateCurrent = new Date();
  //   this.historia_clinica.fecha_nacimiento = new Date( `${event.value}`);
  //   if(dateCurrent.getMonth() > this.historia_clinica.fecha_nacimiento.getMonth()){      
  //       this.historia_clinica.edad = dateCurrent.getFullYear()-this.historia_clinica.fecha_nacimiento.getFullYear();
  //   }
  //   else if (dateCurrent.getMonth() == this.historia_clinica.fecha_nacimiento.getMonth()){
  //     if(dateCurrent.getDate() >= this.historia_clinica.fecha_nacimiento.getDate()){ 
  //       this.historia_clinica.edad = dateCurrent.getFullYear()-this.historia_clinica.fecha_nacimiento.getFullYear();
  //     }else{
  //       this.historia_clinica.edad = (dateCurrent.getFullYear()-this.historia_clinica.fecha_nacimiento.getFullYear())-1;
  //     }  
  //   }else{
  //     this.historia_clinica.edad = (dateCurrent.getFullYear()-this.historia_clinica.fecha_nacimiento.getFullYear())-1;
  //   }    
  // }

  formSubmit(){    
    if(this.historia_clinica.fecha_nacimiento == null){
      this.snack.open('La fecha de Nacimiento es requerida !!','Aceptar',{
        duration : 3000,
        verticalPosition : 'top',
        horizontalPosition : 'right'
      });
      return;
    }
    if(this.historia_clinica.nombre == '' || this.historia_clinica.nombre == null){
      this.snack.open('El nombre es requerido !!','Aceptar',{
        duration : 3000,
        verticalPosition : 'top',
        horizontalPosition : 'right'
      });
      return;
    }
    if(this.historia_clinica.apellido == '' || this.historia_clinica.apellido == null){
      this.snack.open('El apellido es requerido !!','Aceptar',{ 
        duration : 3000,
        verticalPosition : 'top',
        horizontalPosition : 'right'
      });
      return;
    }
 
    if(this.historia_clinica.telefono == '' || this.historia_clinica.telefono == null){
      this.snack.open('El telefono es requerido !!','Aceptar',{
        duration : 3000,
        verticalPosition : 'top',
        horizontalPosition : 'right'
      });
      return;
    }
    if(this.historia_clinica.codigo_departamento == undefined || this.historia_clinica.codigo_departamento == null){
      this.snack.open('El departamento es requerido !!','Aceptar',{
        duration : 3000,
        verticalPosition : 'top',
        horizontalPosition : 'right'
      });
      return;
    }  
//// Enviar Historia Clinica
    this.HistoriaClinicaService.añadirHistoria(this.historia_clinica).subscribe(
      (data) => {
        console.log("Historia Clinica --->",data);
        // enviar Diagnosticos
        this.HistoriaClinicaService.enviarDiagnostico(this.historia_clinica.diagnostico).subscribe(
          (data) => {
            console.log("Diagnosticos --->",data);         
          },(error) => {
            console.log(error);
            this.snack.open('Ha ocurrido un error en el sistema !!','Aceptar',{
              duration : 3000
            });
          }
        )
        // Enviar Tratamientos
        this.HistoriaClinicaService.enviarTratamiento(this.historia_clinica.tratamiento).subscribe(
          (data) => {
            console.log("Tratamientos --->",data);         
          },(error) => {
            console.log(error);
            this.snack.open('Ha ocurrido un error en el sistema !!','Aceptar',{
              duration : 3000
            });
          }
        )
      // Enviar Logs de uso
      let loginTime = new Date();
          let loginTimeString = loginTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true });
          this.ipService.getIpAddress().subscribe(response => {   
            let userStr = localStorage.getItem('user');  
            if(userStr != null){
              let user = JSON.parse(userStr);
              let ipAddress = JSON.stringify(response);
              this.accesoslogs.nombre=user.nombre
              this.accesoslogs.apellido=user.apellido
              this.accesoslogs.identificacion=user.identificacion
              this.accesoslogs.username=user.username
              this.accesoslogs.ip=ipAddress
              this.accesoslogs.accion="Registro Historia Clinica"
              this.accesoslogs.hora=loginTimeString
              this.accesoslogs.fecha=loginTime.toLocaleDateString()
              this.accesousuariosService.enviarLogsAcceso(this.accesoslogs).subscribe((logs) =>{
                
            })
            }                 
            });


        Swal.fire('Registro guardado','La Historia a sido registrada con exito en el sistema','success');
      },(error) => {
        console.log(error);
        this.snack.open('Ha ocurrido un error en el sistema !!','Aceptar',{
          duration : 3000
        });
      }
    )
  }

  exit(){      
    Swal.fire({
      title: '¿Está seguro?',
      text: "¡Saldra del modulo de Registro!",
      icon: 'warning',
      confirmButtonColor: '#3d9970',
      showConfirmButton: true,
      showCancelButton: true,
      denyButtonText: `Don't save`,
      confirmButtonText: 'Ok',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigateByUrl('/'); 
      }        
    })
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  
}
