import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { historia_clinica } from 'src/app/models/historia_clinica';
import { DiagnosticoService } from 'src/app/services/diagnosticos.service';
import { HistoriaClinicaService } from 'src/app/services/historia_clinica.service';
import { TratamientoService } from 'src/app/services/tratamientos.service';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './buscarhistoria.component.html',
  styleUrls: ['./buscarhistoria.component.css']
})
export class BuscarHistoriaComponent implements OnInit {

  public consulta!: FormGroup;
  public listdiagnosticos: any[] = [];
  public listtratamientos: any[] = [];
  public bandera: boolean = false;
  public identificacion!: String;
  public historiasC: any[] = []; 
  public totalH = 0;
  public page = 0;
  public rowsOnPage = 1;
  public math = Math.ceil;
  public panelOpenState = true;



  constructor(private formBuilder: FormBuilder,
    private historia_cl: HistoriaClinicaService,
    private diagnosticoservice: DiagnosticoService,
    private snack: MatSnackBar,
    private tratamientoservice:TratamientoService,
    private router: Router,) { }

  ngOnInit(): void {
    this.panelOpenState=true;
    let user = this.getUser();
    this.bandera = false;
    this.buildForm();
    if (user.authorities[0].authority == "paciente") {
      this.consultahc();
    }
  }


  buildForm(): void {
    this.consulta = this.formBuilder.group({
      identificacionbusqueda: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]],
    })
  }

  public getUser() {
    let userStr = localStorage.getItem('user');
    if (userStr != null) {
      return JSON.parse(userStr);
    } else {
      return null;
    }
  }
  
  consultahc() {   
    this.panelOpenState=true; 
    let user = this.getUser();
    if (user.authorities[0].authority == "paciente") {
      this.identificacion = user.identificacion;
      this.bandera = true;
      //consulta al Backend
      this.historia_cl.getHistoria(this.identificacion).subscribe(
        data => {
          this.totalH = data.length;
            this.diagnosticoservice.getDiagnosticosxHistoria(data[0].id).subscribe(
              diagnos => { 
                this.listdiagnosticos = diagnos;
              })
              this.tratamientoservice.getTratamientoxHistoria(data[0].id).subscribe(
                tratamien => { 
                  this.listtratamientos = tratamien;
                })                                   
          this.historiasC = data.reverse();
        }, error => {
          console.log(error);
        })
        //-------------------------------------busqueda -----------------------
    } else {
      if (this.consulta.controls['identificacionbusqueda'].value == '' || this.consulta.controls['identificacionbusqueda'].value == null) {
        this.snack.open('La identificacion es requerida !!', 'Aceptar', {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right'
        });
        return;
      } else {        
        this.identificacion = this.consulta.controls['identificacionbusqueda'].value
        //consulta al Backend
      this.historia_cl.getHistoria(this.identificacion).subscribe(
        data => {           
          this.historiasC=data           
          if(data.length<=0) //menor o igual, no hay historias
          {
            this.snack.open('No se encontraron datos !!', 'Aceptar', {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right'
            });
            return;
          }else{ // envia a la info de las historiaqs
            this.bandera = true;
            this.totalH = data.length;
            console.log(data)
            this.diagnosticoservice.getDiagnosticosxHistoria(this.historiasC[0].id).subscribe(
              diagnos => {                      
                this.listdiagnosticos = diagnos ;              
              })
              this.tratamientoservice.getTratamientoxHistoria(this.historiasC[0].id).subscribe(
                tratamien => { 
                  this.listtratamientos = tratamien;
                })                     
          }     
        }, error => {
          console.log(error);
        })            
      }      
    }  
  }

  exit() {
    let user = this.getUser();
    if (user.authorities[0].authority == "paciente") {
      Swal.fire({
        title: '¿Está seguro?',
        text: "¡Saldra del modulo de Busqueda!",
        icon: 'warning',
        confirmButtonColor: '#3d9970',
        showConfirmButton: true,
        showCancelButton: true,
        denyButtonText: `Don't save`,
        confirmButtonText: 'Ok',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then((result) => {
        if (result.isConfirmed) {
          this.router.navigateByUrl('/'); 
        }        
      })
    } else {
      if (this.bandera == true) {
        this.bandera = false;
      }
      else {
        Swal.fire({
          title: '¿Está seguro?',
          text: "¡Saldra del modulo de Busqueda!",
          icon: 'warning',
          confirmButtonColor: '#3d9970',
          showConfirmButton: true,
          showCancelButton: true,
          denyButtonText: `Don't save`,
          confirmButtonText: 'Ok',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          }
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigateByUrl('/'); 
          }        
        })
      }
      this.consulta.reset();
    }
  }

  pageChange(p: number): void {
    this.page = p;
  }

  nextPage() {
    if (this.page < this.totalH - this.rowsOnPage) {
      this.page += 1;
      this.diagnosticoservice.getDiagnosticosxHistoria(this.historiasC[this.page].id).subscribe(
        diagnos => {                     
          this.listdiagnosticos = diagnos ;              
        })
        this.tratamientoservice.getTratamientoxHistoria(this.historiasC[this.page].id).subscribe(
          tratamien => { 
            this.listtratamientos = tratamien;
          }) 
    }
  }

  prevPage() {
    if (this.page > 0) {
      this.page -= 1;
      this.diagnosticoservice.getDiagnosticosxHistoria(this.historiasC[this.page].id).subscribe(
        diagnos => {                       
          this.listdiagnosticos = diagnos ;              
        })
        this.tratamientoservice.getTratamientoxHistoria(this.historiasC[this.page].id).subscribe(
          tratamien => { 
            this.listtratamientos = tratamien;
          })
    }
  }
}
