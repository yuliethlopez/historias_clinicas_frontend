import { Router } from '@angular/router';
import { LoginService } from './../../services/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit } from '@angular/core';
import { AccesoUsuariosService } from 'src/app/services/accesousuarios.service';
import { accesousuarios } from 'src/app/models/accesousuarios';
import { IpService } from 'src/app/services/Ip.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData = {
    "username" : '',
    "password" : '',
  }
  

  public ipAddress!: string ;

  constructor(private snack:MatSnackBar,
              private ipService: IpService,
              public accesoslogs: accesousuarios,
              private loginService:LoginService,
              private accesousuariosService:AccesoUsuariosService,
              private router:Router) { }

  ngOnInit(): void {
        
  }

  formSubmit(){
    
    if(this.loginData.username.trim() == '' || this.loginData.username.trim() == null){
      this.snack.open('El nombre de usuario es requerido !!','Aceptar',{
        duration:3000
      })
      return;
    }

    if(this.loginData.password.trim() == '' || this.loginData.password.trim() == null){
      this.snack.open('La contraseña es requerida !!','Aceptar',{
        duration:3000
      })
      return;
    }

    //conexion con backend
    this.loginService.generateToken(this.loginData).subscribe(
      (data:any) => {
        this.loginService.loginUser(data.token);
        this.loginService.getCurrentUser().subscribe((user:any) => {
        this.loginService.setUser(user);       
        let loginTime = new Date();
          let loginTimeString = loginTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true });
          this.ipService.getIpAddress().subscribe(response => {          
          this.ipAddress = JSON.stringify(response);

          this.accesoslogs.nombre=user.nombre
          this.accesoslogs.apellido=user.apellido
          this.accesoslogs.identificacion=user.identificacion
          this.accesoslogs.username=user.username
          this.accesoslogs.ip=this.ipAddress
          this.accesoslogs.accion="Login"
          this.accesoslogs.hora=loginTimeString
          this.accesoslogs.fecha=loginTime.toLocaleDateString()

        this.accesousuariosService.enviarLogsAcceso(this.accesoslogs).subscribe((logs) =>{
          
        })
        }); 
          if(this.loginService.getUserRole() == 'ADMIN'){
            this.snack.open('Bienvenido!!', this.loginService.getUserNombre(),{
              duration:3000,
              verticalPosition: 'top',
              horizontalPosition: 'right'
            })
            this.router.navigate(['']);
            this.loginService.loginStatusSubjec.next(true);
          }
          else if(this.loginService.getUserRole() == 'paciente'){
            this.snack.open('Bienvenido!!', this.loginService.getUserNombre(),{
              duration:3000,
              verticalPosition: 'top',
              horizontalPosition: 'right'
            })
            this.router.navigate(['']);
            this.loginService.loginStatusSubjec.next(true);
          }
          else{
            this.loginService.logout();
          }          
        })
      },(error) => {
        console.log(error);
        this.snack.open('Datos inválidos , vuelva a intentar !!','Aceptar',{
          duration:3000
        })
      }
    )
  }
}
