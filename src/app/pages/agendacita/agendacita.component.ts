import { Component, OnInit } from '@angular/core'; //librerias
import { Router } from '@angular/router';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-agendacita',
  templateUrl: './agendacita.component.html',
  styleUrls: ['./agendacita.component.css']
})
export class AgendacitaComponent implements OnInit { // para inicializar variables, y el ngOnInit para inicializar o ejecutar tareras 

  constructor(private router:Router) { }

  ngOnInit(): void {
  }        
  
  agendar(){
    Swal.fire('Cita Agendada','La Cita se agendo para el dia 07 de Marzo del 2023 a las 3 p.m','success');
  }

  inicio(){
    this.router.navigateByUrl('/'); 
  }

}
