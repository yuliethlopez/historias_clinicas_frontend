import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { user } from 'src/app/models/user';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { DepartamentosService } from 'src/app/services/departamentos.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public listamunicipios: any[] = [];
  public listadepartamentos: any[] = [];
  public listadoeps: any[] = [
    "ALIANSALUD ENTIDAD PROMOTORA DE SALUD S.A.",
    "ASOCIACIÓN INDÍGENA DEL CAUCA",
    "COMFENALCO  VALLE  E.P.S.",
    "COMPENSAR   E.P.S.",
    "COOPERATIVA DE SALUD Y DESARROLLO INTEGRAL ZONA SUR ORIENTAL DE CARTAGENA",
    "E.P.S.  FAMISANAR  LTDA.",
    "E.P.S.  SANITAS  S.A.",
    "EPS SERVICIO OCCIDENTAL DE SALUD  S.A.",
    "EPS Y MEDICINA PREPAGADA SURAMERICANA S.A",
    "MALLAMAS",
    "FUNDACIÓN SALUD MIA EPS",
    "NUEVA EPS S.A.",
    "SALUD  TOTAL  S.A.  E.P.S.",
    "SALUDVIDA S.A .E.P.S",
    "SAVIA SALUD EPS"
  ]
    

  constructor(
    private userService: UserService,
    private snack: MatSnackBar,
    private DepartamentosService:DepartamentosService,
    private router: Router,
    public user:user
    ) { }


  ngOnInit(): void {
     this.TraerInfoNecesariaBack()
  }

  formSubmit() {
    // console.log(this.user);
    if (this.user.username == '' || this.user.username == null) {
      this.snack.open('El nombre de usuario es requerido !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.password == '' || this.user.password == null) {
      this.snack.open('La contraseña es requerida !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.nombre == '' || this.user.nombre == null) {
      this.snack.open('El nombre es requerido !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.apellido == '' || this.user.apellido == null) {
      this.snack.open('El apellido es requerido !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.identificacion == '' || this.user.identificacion == null) {
      this.snack.open('La identificacion es requerida !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.fecha_nacimiento == null ||this.user.fecha_nacimiento) {
      this.snack.open('La fecha de nacimiento es requerida !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.lugar_nacimiento == '' || this.user.lugar_nacimiento == null) {
      this.snack.open('El lugar de nacimiento es requerido !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.telefono == '' || this.user.telefono == null) {
      this.snack.open('El telefono es requerido !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.sexo == '' || this.user.sexo == null) {
      this.snack.open('El sexo es requerida !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.eps == '' || this.user.eps == null) {
      this.snack.open('La EPS es requerida !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.direccion == '' || this.user.direccion == null) {
      this.snack.open('La dirección es requerida !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.regimen == '' || this.user.regimen == null) {
      this.snack.open('El regimen es requerido !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.estado_civil == '' || this.user.estado_civil == null) {
      this.snack.open('Estado civil es requerido !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.edad == 0 || this.user.edad == null) {
      this.snack.open('La edad es requerida !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }
    if (this.user.email == '' || this.user.email == null) {
      this.snack.open('Email es requerido !!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      return;
    }

    //Conexion Backend
    this.userService.añadirUsuario(this.user).subscribe(
      (data) => {
        console.log(data);
        Swal.fire('Usuario guardado', 'Usuario registrado con exito en el sistema', 'success');
      }, (error) => {
        console.log(error);
        this.snack.open('Ha ocurrido un error en el sistema !!', 'Aceptar', {
          duration: 3000
        });
      }
    )
  }

  exit() {
    if (this.user.username == null||this.user.username != null) {
      Swal.fire({
        title: '¿Está seguro?',
        text: "¡Si no le diste en Registrar tu usuario no se creara!",
        icon: 'warning',
        confirmButtonColor: '#3d9970',
        showConfirmButton: true,
        showCancelButton: true,
        denyButtonText: `Don't save`,
        confirmButtonText: 'Ok',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then((result) => {
        if (result.isConfirmed) {
          window.location.href = "http://localhost:4200/login"
        }
      })
    }
  }

  buscarmunicipios(codigo_departamento: number){
    this.DepartamentosService.getMunicipioxDepartamento(codigo_departamento).subscribe(
      data => {
        this.listamunicipios = data;
         }, error => {
        console.log(error);
      })
  }

  TraerInfoNecesariaBack(){
    this.DepartamentosService.getDepartamentos().subscribe(
      data => {
        this.listadepartamentos = data;
      }, error => {
        console.log(error);
      })
  }

  addFechadeNacimiento(event: MatDatepickerInputEvent<Date>) {
    let dateCurrent = new Date();
    this.user.fecha_nacimiento = new Date( `${event.value}`);
    if(dateCurrent.getMonth() > this.user.fecha_nacimiento.getMonth()){      
        this.user.edad = dateCurrent.getFullYear()-this.user.fecha_nacimiento.getFullYear();
    }
    else if (dateCurrent.getMonth() == this.user.fecha_nacimiento.getMonth()){
      if(dateCurrent.getDate() >= this.user.fecha_nacimiento.getDate()){ 
        this.user.edad = dateCurrent.getFullYear()-this.user.fecha_nacimiento.getFullYear();
      }else{
        this.user.edad = (dateCurrent.getFullYear()-this.user.fecha_nacimiento.getFullYear())-1;
      }  
    }else{
      this.user.edad = (dateCurrent.getFullYear()-this.user.fecha_nacimiento.getFullYear())-1;
    }    
  }


}
