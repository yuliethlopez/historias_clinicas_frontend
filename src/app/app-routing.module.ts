import { NormalGuard } from './services/normal.guard';
import { AdminGuard } from './services/admin.guard';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NosotrosComponent } from './pages/nosotros/nosotros.component';
import { AgendacitaComponent } from './pages/agendacita/agendacita.component';
import { SignupMedicoComponent } from './pages/signup-medico/signup-medico.component';
import { RegistrarHistoriaComponent } from './pages/registrarhistoria/registrarhistoria.component';
import { BuscarHistoriaComponent } from './pages/buscarhistoria/buscarhistoria.component';



const routes: Routes = [
  {
    path : '',
    component : HomeComponent,
    pathMatch : 'full'
  },
  {
    path : '*',
    component : HomeComponent,
    pathMatch : 'full'
  },
  {
    path : 'signup',
    component : SignupComponent,
    pathMatch : 'full'
  },
  {
    path : 'login',
    component : LoginComponent,
    pathMatch : 'full'
  },
  {
    path : 'nosotros',
    component : NosotrosComponent,
    pathMatch : 'full'
  },
  {
    path : 'cita',
    component : AgendacitaComponent,
    pathMatch : 'full'
  },
  {
    path:'registrar',
    component:RegistrarHistoriaComponent,
    pathMatch:'full',
    canActivate:[AdminGuard]
  },
  {
    path:'busqueda',
    component:BuscarHistoriaComponent,
    pathMatch:'full',
    canActivateChild:[NormalGuard,AdminGuard]
  },

  {
    path:'signupmedico',
    component:SignupMedicoComponent,
    pathMatch:'full',
    canActivateChild:[NormalGuard,AdminGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
