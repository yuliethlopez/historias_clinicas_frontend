export class user{
    username!:string;
    password!:string;
    fecha!:string;
    nombre !:string;
    apellido!:string;
    identificacion!:string;
    lugar_nacimiento!:string;
    fecha_nacimiento!:Date;
    edad!:number;
    sexo!:string;
    eps!:string;    
    direccion!:string;
    codigo_departamento!:{};
    codigo_municipio!:{};
    regimen!:string;
    estado_civil!:string;
    email !:string;
    telefono !:string;
}