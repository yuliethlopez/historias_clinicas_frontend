//formulario de  historia cllinica

import { diagnostico } from "./diagnostico";

export class historia_clinica{
    id!:string;
    fecha!:string;
    nombre !:string;
    apellido!:string;
    identificacion!:string;
    lugar_nacimiento!:string;
    fecha_nacimiento:string | undefined;
    edad!:number;
    sexo!:string;
    eps!:string;    
    direccion!:string;
    codigo_departamento!:{};
    codigo_municipio!:{};
    regimen!:string;
    estado_civil!:string;
    email !:string;
    telefono !:string;
    motivo_consulta!:string;
    examen_fisico_Neurologico!:string;
    examen_fisico_Cardiovascular!:string;
    examen_fisico_Respiratorio!:string;
    examen_fisico_Musculoesqueletico!:string;
    examen_fisico_Genitourinario!:string;
    examen_fisico_Tegumentario!:string;
    presion_arterial!:string;
    frecuencia_cardiaca!:string;
    frecuencia_respiratoria!:string;
    temperatura!:string;
    peso!:number;
    talla!:number;
    imc!:number;
    evolucion!:string;
    diagnostico!:{}; 
    tratamiento!:{};
    observaciones!:string;
    recomendaciones!:string;
    remision!:string;
}